all: build
start: dev

build:
	npm run build
dev:
	npm run start
lint:
	npm run lint
