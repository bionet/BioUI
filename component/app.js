(function() {
  var React = require('react');
  var ReactDOM = require('react-dom');
  var Main = require('./main/Main.js');
  var remote = window.require('remote');
  var target = document.getElementById("MainApp");

  window.React = React;

  ReactDOM.render(<Main />, target);
})();
