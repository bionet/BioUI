const React = require('react');
const MUIAppBar = require('material-ui/lib/app-bar');

const AppBar = React.createClass({
  render: function() {
    return (
       <MUIAppBar title="Title"
          iconClassNameRight="muidocs-icon-navigation-expand-more" />
    );
  }
});

module.exports = AppBar;
