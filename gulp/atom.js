var gulp = require('gulp');
var atom = require('gulp-atom');

gulp.task('atom', function() {
  atom({
    srcPath: './compile',
    releasePath: './build',
    cachePath: './cache',
    version: 'v0.34.0',
    rebuild: false,
    platforms: [
      'darwin-x64',
      'linux-x64'
    ],
  });
});
