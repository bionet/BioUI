var gulp = require( 'gulp' );
var browserify = require( 'browserify' );
var babelify = require( 'babelify' );
var reactify = require( 'reactify' );
var source = require( 'vinyl-source-stream' );

gulp.task( 'browserify', function () {
	var bundler = browserify({debug: true, entries: ['./component/app.js']})
	.transform("babelify", {presets: ["react"]})
	.transform("reactify");

	return bundler.bundle()
		.pipe( source( 'app.js' ) )
		.pipe( gulp.dest( './compile' ) );
});
